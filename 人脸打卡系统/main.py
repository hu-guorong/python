import shutil
import threading
import cv2
import os
import numpy as np
import tkinter as tk
import time
from tkinter import simpledialog, messagebox, filedialog
from PIL import Image, ImageTk
import sqlite3
import pyttsx3
def create_database():
    # 连接数据库，如果不存在则会创建一个新的数据库文件
    conn = sqlite3.connect("face_database.db")
    # 创建一个游标对象来执行 SQL 语句
    c = conn.cursor()
    # 创建名为 "faces" 的表，用于姓名
    c.execute('''CREATE TABLE IF NOT EXISTS faces
                 (name varchar(20) PRIMARY KEY,
                 t  varchar(20)
                )''')
    # 提交更改并关闭连接
    conn.commit()
    conn.close()

def updata_database(name):
    #t = time.strftime("%Y-%m-%d %H:%M:%S", time.localtime())
    t = time.strftime("%H:%M:%S", time.localtime())
    conn = sqlite3.connect("face_database.db")
    cursor = conn.cursor()
    cursor.execute("SELECT 1 FROM faces WHERE name=?", (name,))
    if cursor.fetchone() is None:
        insert_sql = '''INSERT INTO faces (name, t) VALUES (?, ?)'''
        cursor.execute(insert_sql, (name,t))
    else:
        sql_update = 'UPDATE faces SET t=? WHERE name=?'
        data_to_insert = (t, name)  # 使用参数化查询，这有助于防止SQL注入
        cursor.execute(sql_update, data_to_insert)
    conn.commit()
    cursor.close()
    conn.close()
    print("打开记录已更新")

class FaceRecognitionApp:
    def __init__(self, window, window_title):
        self.window = window
        self.window.title(window_title)
        self.cap = cv2.VideoCapture(0)  # 打开摄像头
        self.engine = pyttsx3.init()  # 初始化pyttsx3引擎
        self.speek = True
        self.face_cascade = cv2.CascadeClassifier(cv2.data.haarcascades + 'haarcascade_frontalface_default.xml')
        self.face_recognizer = cv2.face.LBPHFaceRecognizer_create()
        self.trained = False  # 标记模型是否已经训练
        self.labels = {}  # 存储ID和标签的映射
        self.load_model()  # 加载模型和标签
        # 创建画布
        self.canvas = tk.Canvas(window, width=640, height=480)
        self.canvas.pack()
        # 按钮 - 录入人脸
        self.add_button = tk.Button(window, text="录入人脸", command=self.add_person)
        self.add_button.pack()
        # 按钮 - 管理人脸
        self.manage_button = tk.Button(window, text="管理人脸", command=self.manage_persons)
        self.manage_button.pack()
        self.update()  # 开始更新GUI

    def load_model(self):   #让人脸和标签对应上
        model_filename = 'faces/LBPH.xml'
        if os.path.exists(model_filename):
            self.face_recognizer.read(model_filename)
            # 从faces目录下的子文件夹中加载ID和标签
            self.labels = {label: d for label, d in enumerate(os.listdir('faces')) if
                           os.path.isdir(os.path.join('faces', d))}
            self.trained = True
            print("模型加载成功。")
        else:
            print("模型文件不存在，需要训练模型。")
    #@staticmethod
    def speak_daka(self, message):
        try:
            self.engine.say(message)  # 将语音消息添加到队列
            self.engine.runAndWait()
        except Exception as e:
            pass
            #print(e)   -->>>>run loop already started
    def update(self):
        ret, frame = self.cap.read()
        if not ret:
            print("无法从摄像头读取数据。")
            return  # 退出函数，不执行后续操作
        gray = cv2.cvtColor(frame, cv2.COLOR_BGR2GRAY)
        faces = self.face_cascade.detectMultiScale(gray, 1.3, 5)
        if self.trained:
            for (x, y, w, h) in faces:
                face_img = gray[y:y + h, x:x + w]
                face_img = cv2.resize(face_img, (200, 200))
                label, confidence = self.face_recognizer.predict(face_img)
                # 根据confidence值判断是否为已知人脸
                if confidence < 100:  # 假设100为confidence阈值
                    cv2.rectangle(frame, (x, y), (x + w, y + h), (0, 255, 0), 2)
                    # 使用self.labels字典获取人员名称并显示
                    person_name = self.labels.get(label)
                    cv2.putText(frame, f"Person: {person_name}", (x, y - 30),
                                cv2.FONT_HERSHEY_SIMPLEX, 0.9, (0, 255, 0), 2)
                    #数据库部分  ->>上传
                    updata_database(person_name)
                    #pyttsx3.speak('打卡成功')   卡顿
                    if self.speek :
                        threading.Thread(target=self.speak_daka, args=('打卡成功',)).start()

                    #self.speak_daka("打卡成功")

                else:
                    cv2.rectangle(frame, (x, y), (x + w, y + h), (0, 0, 255), 2)
                    cv2.putText(frame, "Unknown", (x, y - 10),
                                cv2.FONT_HERSHEY_SIMPLEX, 0.9, (0, 0, 255), 2)
        else:
            # 如果模型未训练，只显示圈出的人脸并标注 "no"
            for (x, y, w, h) in faces:
                cv2.rectangle(frame, (x, y), (x+w, y+h), (0, 0, 255), 2)
                cv2.putText(frame, "no", (x, y-10), cv2.FONT_HERSHEY_SIMPLEX, 0.9, (0, 0, 255), 2)
        # 显示图像
        img = Image.fromarray(cv2.cvtColor(frame, cv2.COLOR_BGR2RGB))
        imgtk = ImageTk.PhotoImage(image=img)
        self.canvas.create_image(0, 0, image=imgtk, anchor=tk.NW)
        self.photo = imgtk
        self.window.after(20, self.update)  # 递归更新画面
    def add_person(self):
        def on_name_provided(name):
            if name:
                folder_path = os.path.join('faces', name)
                if not os.path.exists(folder_path):
                    os.makedirs(folder_path)
                    # 正确地调用实例方法，不传递 self
                    self.capture_faces(folder_path, name)
                else:
                    messagebox.showerror("错误", "该人员已存在！")

        def get_name():
            name = simpledialog.askstring("输入名称", "请输入人员名称:")
            if name:  # 确保名称不为空
                threading.Thread(target=on_name_provided, args=(name,)).start()

        # 使用 after 在主线程中调用 get_name
        self.window.after(0, get_name)

    def capture_faces(self, folder_path, name):
        self.speek =False
        capture_count = 0  # 用于跟踪已保存的照片数量
        while capture_count < 100:  #截图数量
            ret, frame = self.cap.read()
            if not ret:
                messagebox.showerror("错误", "无法从摄像头读取数据。")
                break

            gray = cv2.cvtColor(frame, cv2.COLOR_BGR2GRAY)
            faces = self.face_cascade.detectMultiScale(gray, 1.3, 5)

            if len(faces) == 0:
                continue

            (x, y, w, h) = faces[0]
            face_img = gray[y:y + h, x:x + w]
            cv2.imwrite(os.path.join(folder_path, f"{name}_{capture_count:03d}.jpg"), face_img)
            capture_count += 1

            # 在图像上绘制已保存的照片数量
            frame = cv2.putText(frame, f"Saved: {capture_count}/100", (10, 30),
                                cv2.FONT_HERSHEY_SIMPLEX, 1, (0, 255, 0), 2)

            # 显示图像
            img = Image.fromarray(cv2.cvtColor(frame, cv2.COLOR_BGR2RGB))
            imgtk = ImageTk.PhotoImage(image=img)
            self.canvas.create_image(0, 0, image=imgtk, anchor=tk.NW)
            self.photo = imgtk

            # 这里使用短时间的延迟，以避免CPU占用过高
            cv2.waitKey(10)
        #pyttsx3.speak('人脸照片采集完成')   ---->>>会卡顿
        threading.Thread(target=self.speak_daka, args=('人脸照片采集完成',)).start()
        messagebox.showinfo("信息", f"{name} 的人脸照片采集完成，共 {capture_count} 张。")
        # 捕获完成后训练模型
        self.train_model()
        self.speek = True
    def train_model(self):
        try:
            # 获取所有子文件夹（假设每个子文件夹是一个人员的人脸图像）
            face_directories = [d for d in os.listdir('faces') if os.path.isdir(os.path.join('faces', d))]
            faces_data = []
            labels_data = []
            # 准备训练数据
            for label, directory in enumerate(face_directories):
                dir_path = os.path.join('faces', directory)
                for filename in os.listdir(dir_path):
                    img_path = os.path.join(dir_path, filename)
                    img = cv2.imread(img_path, cv2.IMREAD_GRAYSCALE)
                    if img is not None:
                        # 确保所有图像具有相同的尺寸，例如 200x200
                        img_resized = cv2.resize(img, (200, 200))
                        faces_data.append(img_resized)
                        labels_data.append(label)
            # 将标签列表转换为 numpy 数组
            labels_data = np.array(labels_data)
            # 转换 faces_data 为形状为 (n, h, w) 的二维数组
            faces_data = np.array(faces_data)
            # 训练模型
            self.face_recognizer.train(faces_data, labels_data)
            print("模型训练完成。")
            # 保存模型到文件
            model_filename = 'faces/LBPH.xml'
            self.face_recognizer.save(model_filename)  # 使用 save 方法保存模型
            print(f"模型已保存到：{model_filename}")
            self.load_model()   #---》重新更新标签 让其对应
            self.trained = True
        except Exception as e:
            print(f"模型训练失败：{e}")
    def manage_persons(self):
        def delete_selected_folders(selected_folders):
            print("要删除的文件夹路径：", selected_folders)
            for folder in selected_folders:
                folder_path = os.path.join('faces', folder)
                if os.path.isdir(folder_path):
                    try:
                        shutil.rmtree(folder_path)  # 删除文件夹
                        messagebox.showinfo("信息", f"已删除：{folder}")
                    except Exception as e:
                        messagebox.showerror("错误", f"无法删除 {folder}：{e}")

            self.train_model()  # 删除后重新训练模型

        # 打开文件夹选择对话框
        selected_folder = filedialog.askdirectory(title="选择人员文件夹", initialdir='faces')
        if selected_folder:
            # 检查选择的是否是一个有效目录
            if not os.path.isdir(selected_folder):
                messagebox.showerror("错误", "所选路径不是有效的文件夹")
                return
            # 确认删除
            if messagebox.askyesno("确认", f"您确定要删除文件夹 {selected_folder} 吗？"):
                delete_selected_folders([selected_folder])
    def on_closing(self):
        self.cap.release()
        self.window.destroy()

if __name__ == "__main__":
    create_database()
    window = tk.Tk()
    app = FaceRecognitionApp(window, "人脸识别系统")
    window.protocol("WM_DELETE_WINDOW", app.on_closing)
    window.mainloop()
