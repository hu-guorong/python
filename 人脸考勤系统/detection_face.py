import cv2
import face_recognition

# 打开摄像头
video_capture = cv2.VideoCapture(0)

count = 0  # 用于生成唯一的文件名

while True:
    # 读取摄像头画面
    ret, frame = video_capture.read()

    # 将画面转换为 RGB 格式
    rgb_frame = frame[:, :, ::-1]

    # 检测人脸
    face_locations = face_recognition.face_locations(rgb_frame)

    # 在画面中标识人脸并保存人脸图片
    for top, right, bottom, left in face_locations:
        cv2.rectangle(frame, (left, top), (right, bottom), (0, 0, 255), 2)
        # 提取人脸图片
        face_image = frame[top:bottom, left:right]
        # 保存人脸图片，文件名加上计数器以确保唯一性
        #cv2.imwrite(f"face_img/face_image_{count}.jpg", face_image)
        #count += 1  # 更新计数器‘
        print(face_image)

    # 显示画面
    cv2.imshow('Video', frame)

    # 按下 'q' 键退出循环
    if cv2.waitKey(1) & 0xFF == ord('q'):
        break

# 关闭摄像头
video_capture.release()
cv2.destroyAllWindows()
