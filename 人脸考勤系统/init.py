import tkinter as tk
import cv2
from tkinter import simpledialog, Listbox
from PIL import Image, ImageTk
import sqlite3
import os
import pyttsx3
import face_recognition

def create_database():
    conn = sqlite3.connect("face_database.db")
    c = conn.cursor()
    c.execute('''CREATE TABLE IF NOT EXISTS faces
                 (id INTEGER PRIMARY KEY,
                  name TEXT NOT NULL,
                  addr TEXT)''')
    conn.commit()
    conn.close()

class FaceRecognitionApp:
    def __init__(self, root):
        self.root = root
        self.root.title("Face Recognition App")
        self.camera_frame = tk.Label(self.root)
        self.camera_frame.pack(side=tk.LEFT)
        self.button_frame = tk.Frame(self.root)
        self.button_frame.pack(side=tk.RIGHT)
        self.img_value = None
        self.info_button = tk.Button(self.button_frame, text="人脸信息", command=self.show_face_info)
        self.info_button.pack(pady=10)
        self.register_button = tk.Button(self.button_frame, text="录入人脸", command=self.register_face)
        self.register_button.pack(pady=10)
        # Attempt to open the video capture device
        self.video_capture = cv2.VideoCapture(0)
        if not self.video_capture.isOpened():
            print("__init__-->>>>无法打开摄像头设备。")
            self.video_capture.release()
            self.video_capture = None
        else:
            self.db_conn = sqlite3.connect("face_database.db")
            self.db_cursor = self.db_conn.cursor()
            self.update_camera()

    def update_camera(self):
        if self.video_capture is None:
            print("update_camera->>>>>摄像头未打开，无法更新画面。")
            return
        #print(self.video_capture.read())
        ret,frame = self.video_capture.read()
        if not ret:
            print("update_camera->>>>>无法从摄像头读取数据。")
            return
        if ret:
            rgb_frame = frame[:, :, ::-1]
            rgb_frame = rgb_frame.copy()
            face_locations = face_recognition.face_locations(rgb_frame)
            face_encodings = face_recognition.face_encodings(rgb_frame, face_locations)
            for (top, right, bottom, left), face_encoding in zip(face_locations, face_encodings):
                match_name = self.match_face(face_encoding)
                cv2.rectangle(frame, (left, top), (right, bottom), (0, 255, 0), 2)
                cv2.putText(frame, match_name, (left + 6, bottom - 6), cv2.FONT_HERSHEY_SIMPLEX, 0.5, (0, 255, 0), 1)
                self.img_value = frame[top:bottom, left:right]
            rgb_image = cv2.cvtColor(frame, cv2.COLOR_BGR2RGB)
            pil_image = Image.fromarray(rgb_image)
            max_width = 640
            if pil_image.width > max_width:
                scale_factor = max_width / pil_image.width
                pil_image = pil_image.resize((int(pil_image.width * scale_factor), int(pil_image.height * scale_factor)))
            tk_image = ImageTk.PhotoImage(pil_image)
            self.camera_frame.configure(image=tk_image)
            self.camera_frame.image = tk_image
        self.root.after(10, self.update_camera)


    def match_face(self, face_encoding):
        self.db_cursor.execute("SELECT name, addr FROM faces")
        known_faces = self.db_cursor.fetchall()
        match_name = "Unknown"
        for name, addr in known_faces:
            try:
                known_image = face_recognition.load_image_file(addr)
                known_face_encodings = face_recognition.face_encodings(known_image)
                if known_face_encodings:
                    result = face_recognition.compare_faces([known_face_encodings[0]], face_encoding)
                    if result[0]:
                        match_name = name
                        break
            except Exception as e:
                print(f"Error processing {name}: {e}")
        return match_name

    def show_face_info(self):
        self.db_cursor.execute("SELECT * FROM faces")
        results = self.db_cursor.fetchall()
        window = tk.Toplevel(self.root)
        window.title("人脸信息")
        listbox = Listbox(window, width=40)
        listbox.pack(padx=20, pady=20)
        for row in results:
            face_info = f"ID: {row[0]}, Name: {row[1]}"
            listbox.insert(tk.END, face_info)
        delete_button = tk.Button(window, text="删除", command=lambda: self.delete_face(listbox, results))
        delete_button.pack()

    def delete_face(self, listbox, results):
        try:
            selected_index = listbox.curselection()
            if selected_index:
                selected_index = selected_index[0]
                # 检查results列表是否为空
                if results:
                    selected_id = results[selected_index][0]
                    self.db_cursor.execute("SELECT addr FROM faces WHERE id=?", (selected_id,))
                    addr = self.db_cursor.fetchone()[0]
                    if os.path.exists(addr):
                        os.remove(addr)
                    self.db_cursor.execute("DELETE FROM faces WHERE id=?", (selected_id,))
                    self.db_conn.commit()
                    listbox.delete(selected_index)
                    # 更新results列表
                    self.results = [row for row in results if row[0] != selected_id]
                    pyttsx3.speak("删除成功")
                    print("数据库删除成功")
                else:
                    pyttsx3.speak("数据库内无数据")
            else:
                pyttsx3.speak("请先选择一个项目")
        except Exception as e:
            pyttsx3.speak("删除失败，请检测设备数据")
            print(f"删除人脸信息时发生错误：{e}")

    def register_face(self):
        name = simpledialog.askstring("输入姓名", "请输入姓名：")
        if name and self.img_value is not None:
            file_path = f"face_img/face_{name}.jpg"
            cv2.imwrite(file_path, self.img_value)
            self.db_cursor.execute("INSERT INTO faces (name, addr) VALUES (?, ?)", (name, file_path))
            self.db_conn.commit()
            pyttsx3.speak("人脸信息已录入数据库")
            print("人脸信息已录入数据库！")

    def on_closing(self):
        if self.video_capture is not None:
            self.video_capture.release()
        self.db_conn.close()
        self.root.destroy()

if __name__ == "__main__":
    pyttsx3.speak("欢迎使用人脸识别系统")
    create_database()
    root = tk.Tk()
    app = FaceRecognitionApp(root)
    root.protocol("WM_DELETE_WINDOW", app.on_closing)
    root.mainloop()