import sqlite3

def create_database():
    # 连接数据库，如果不存在则会创建一个新的数据库文件
    conn = sqlite3.connect("face_database.db")

    # 创建一个游标对象来执行 SQL 语句
    c = conn.cursor()

    # 创建名为 "faces" 的表，用于存储人脸信息和姓名
    c.execute('''CREATE TABLE IF NOT EXISTS faces
                 (id INTEGER PRIMARY KEY,
                  name TEXT NOT NULL,
                  addr  TEXT)''')  #二进制
    # 提交更改并关闭连接
    conn.commit()
    conn.close()

if __name__ == "__main__":
    create_database()
    print("数据库已创建！")
