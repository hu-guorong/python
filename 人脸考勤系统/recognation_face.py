import cv2
import face_recognition

# 加载刚刚保存的人脸图片
try:
    known_image = face_recognition.load_image_file("face_image.jpg")
    known_face_encodings = face_recognition.face_encodings(known_image)
    if known_face_encodings:
        known_face_encoding = known_face_encodings[0]
    else:
        print("No face detected in the known image.")
        exit()

except FileNotFoundError:
    print("Known image file not found.")
    exit()

# 打开摄像头
video_capture = cv2.VideoCapture(0)

if not video_capture.isOpened():
    print("Could not open video device")
    exit()

while True:
    # 读取摄像头画面
    ret, frame = video_capture.read()
    if not ret:
        print("Can't receive frame (stream end?). Exiting ...")
        break

        # 将画面转换为 RGB 格式
    rgb_frame = frame[:, :, ::-1]
    #  要硬拷贝一下 否着报错
    rgb_frame = rgb_frame.copy()
    # 检测画面中的人脸
    face_locations = face_recognition.face_locations(rgb_frame)
    # 如果检测到人脸，则生成面部编码
    face_encodings = face_recognition.face_encodings(rgb_frame, face_locations)
    #print(face_encodings)
    # 在画面中标识人脸
    for (top, right, bottom, left), face_encoding in zip(face_locations, face_encodings):
        # 尝试识别人脸
        matches = face_recognition.compare_faces([known_face_encoding], face_encoding)
        name = "Unknown"
        # 如果识别成功，则标识为 "Known"
        if True in matches:
            name = "Known"
            # 标识人脸
        cv2.rectangle(frame, (left, top), (right, bottom), (0, 0, 255), 2)
        cv2.putText(frame, name, (left + 6, bottom - 6), cv2.FONT_HERSHEY_SIMPLEX, 0.5, (0, 0, 255), 1)

        # 显示画面
    cv2.imshow('Video', frame)

    # 按下 'q' 键退出循环
    if cv2.waitKey(1) & 0xFF == ord('q'):
        break

    # 关闭摄像头
video_capture.release()
cv2.destroyAllWindows()