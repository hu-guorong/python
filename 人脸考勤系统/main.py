import tkinter as tk
import cv2
from tkinter import simpledialog  #输入框
from PIL import Image, ImageTk
import sqlite3
import os
import pyttsx3  #语音库2  ->仅支持文本转语音
import face_recognition
import numpy as np

def create_database():
    # 连接数据库，如果不存在则会创建一个新的数据库文件
    conn = sqlite3.connect("face_database.db")
    # 创建一个游标对象来执行 SQL 语句
    c = conn.cursor()
    # 创建名为 "faces" 的表，用于存储人脸信息和姓名
    c.execute('''CREATE TABLE IF NOT EXISTS faces
                 (id INTEGER PRIMARY KEY,
                  name TEXT NOT NULL,
                  addr  TEXT)''')  #存放人脸图片的路径
    # 提交更改并关闭连接
    conn.commit()
    conn.close()
class FaceRecognitionApp:
    def __init__(self, root):
        self.root = root
        self.root.title("Face Recognition App")
        # 创建左侧画面显示区域
        self.camera_frame = tk.Label(self.root)
        self.camera_frame.pack(side=tk.LEFT)
        # 创建右侧按钮区域
        self.button_frame = tk.Frame(self.root)
        self.button_frame.pack(side=tk.RIGHT)
        self.img_value=""
        # 创建“人脸信息”按钮
        self.info_button = tk.Button(self.button_frame, text="人脸信息", command=self.show_face_info)
        self.info_button.pack(pady=10)
        # 创建“录入人脸”按钮
        self.register_button = tk.Button(self.button_frame, text="录入人脸", command=self.register_face)
        self.register_button.pack(pady=10)
        # 打开摄像头
        self.video_capture = cv2.VideoCapture(0)  # 使用第一个摄像头设备
        # 开始更新摄像头画面
        self.db_conn = sqlite3.connect("face_database.db")  # 在类级别管理数据库连接
        self.db_cursor = self.db_conn.cursor()  # 创建游标对象
        self.update_camera()
    def update_camera(self):
        ret, frame = self.video_capture.read()
        if ret:
            # 将画面转换为 RGB 格式
            rgb_frame = frame[:, :, ::-1]
            #  要硬拷贝一下 否着报错
            rgb_frame = rgb_frame.copy()
            # 检测人脸
            face_locations = face_recognition.face_locations(rgb_frame)
            # 在画面中标识人脸
            face_encodings = face_recognition.face_encodings(rgb_frame, face_locations)
            # 如果检测到人脸，则生成面部编码
            for (top, right, bottom, left), face_encoding in zip(face_locations, face_encodings):
                # 查询数据库中的人脸信息
                conn = sqlite3.connect("face_database.db")
                c = conn.cursor()
                c.execute("SELECT name, addr FROM faces")
                known_faces = c.fetchall()
                conn.close()
                match_name = "Unknown"
                for name, addr in known_faces:
                    try:
                        known_image = face_recognition.load_image_file(addr)
                        known_face_encodings = face_recognition.face_encodings(known_image)
                        known_face_encoding = known_face_encodings[0]
                        result = face_recognition.compare_faces([known_face_encoding], face_encoding,tolerance=0.4)
                        # 比较识别到的人脸编码和数据库中的人脸编码
                        if result[0]:  # 如果匹配成功
                            match_name = name
                            break
                    except Exception as e:
                        print(f"Error processing {name}: {e}")
                        continue
                cv2.rectangle(frame, (left, top), (right, bottom), (0, 0, 255), 2)
                cv2.putText(frame, match_name, (left + 6, bottom - 6), cv2.FONT_HERSHEY_SIMPLEX, 0.5, (0, 0, 255), 1)
                #把人脸信息识别出来
                self.img_value = frame[top:bottom, left:right]
                #print(type(self.img_value))
            # 将 OpenCV 的 BGR 图像转换为 PIL 的 RGB 图像
            rgb_image = cv2.cvtColor(frame, cv2.COLOR_BGR2RGB)
            pil_image = Image.fromarray(rgb_image)
            # 缩放图像以适应界面
            width, height = pil_image.size
            max_width = 640
            if width > max_width:
                scale_factor = max_width / width
                new_width = int(width * scale_factor)
                new_height = int(height * scale_factor)
                pil_image = pil_image.resize((new_width, new_height))
            # 将 PIL 图像转换为 Tkinter 支持的格式
            tk_image = ImageTk.PhotoImage(pil_image)
            # 在界面上显示图像
            self.camera_frame.configure(image=tk_image)
            self.camera_frame.image = tk_image
        # 循环更新画面
        self.root.after(10, self.update_camera)
    def show_face_info(self):
        self.db_cursor.execute("SELECT * FROM faces")
        results = self.db_cursor.fetchall()
        self.results = results  # 确保 results 为类属性
        # 连接数据库
        conn = sqlite3.connect("face_database.db")
        c = conn.cursor()
        # 查询数据库中的人脸信息
        c.execute("SELECT * FROM faces")
        results = c.fetchall()
        # 关闭数据库连接
        conn.close()
        # 创建一个窗口
        window = tk.Tk()
        window.title("人脸信息")
        # 创建一个列表框，用于展示人脸信息
        listbox = tk.Listbox(window, width=40)
        listbox.pack(padx=20, pady=20)
        # 将人脸信息添加到列表框中
        for row in results:
            face_info = f"ID: {row[0]}, Name: {row[1]}"
            listbox.insert(tk.END, face_info)
        # 创建一个删除按钮
        def delete_face():
            try:
                selected_index = listbox.curselection()
                if selected_index:
                    selected_id = results[selected_index[0]][0]
                    #print(selected_id)
                    conn = sqlite3.connect("face_database.db")
                    c = conn.cursor()
                    # 提取地址信息
                    c.execute("SELECT addr FROM faces WHERE id=?", (selected_id,))
                    addr = c.fetchone()[0]  # addr 是数据库中存储地址的字段
                    # 删除图片文件
                    if os.path.exists(addr):
                        os.remove(addr)
                    c.execute("DELETE FROM faces WHERE id=?", (selected_id,))
                    conn.commit()
                    conn.close()
                    listbox.delete(selected_index)
                    self.results = [row for row in self.results if row[0] != selected_id]  # 更新 results
                    pyttsx3.speak("删除成功")
                else :
                    pyttsx3.speak("数据库内无数据")
            except Exception as e:
                pyttsx3.speak("删除失败，请检测设备数据")
                print(f"删除人脸信息时发生错误：{e}")

        delete_button = tk.Button(window, text="删除", command=delete_face)
        delete_button.pack()
        # 运行窗口主循环
        window.mainloop()
    def register_face(self):
        name = simpledialog.askstring("输入姓名", "请输入姓名：")
        cv2.imwrite(f"face_img/face_{name}.jpg",self.img_value)
        addr = f"face_img/face_{name}.jpg"
        if name :
            # 将人脸信息存入数据库
            conn = sqlite3.connect("face_database.db")
            c = conn.cursor()
            c.execute("INSERT INTO faces (name, addr) VALUES (?, ?)", (name, addr))
            conn.commit()
            conn.close()
            pyttsx3.speak("人脸信息已录入数据库")
            print("人脸信息已录入数据库！")

if __name__ == "__main__":
    pyttsx3.speak("欢迎使用人脸识别系统")
    create_database()  # 确保此函数正确创建数据库和表
    root = tk.Tk()
    app = FaceRecognitionApp(root)
    root.mainloop()
    app.db_conn.close()  # 程序结束时关闭数据库连接
